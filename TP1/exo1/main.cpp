#include <iostream>
#include <ctime>
#include <cstdlib>
#include "calculatrice.h"

using namespace std;

int main()
{
    cout << "Hello user!" << endl;
    time_t i = 0;
    srand(time(&i));
    int x1 = rand() % 1001;
    i++;
    srand(time(&i) + 1);
    int x2 {rand() % 1001};
    cout << "Give some : " << x1 << " + " << x2 << endl;
    calculatrice *calc = new calculatrice(x1, x2);
    int result = calc->getResult();
    int some;
    int badAnswer = -1;
    do
    {
        badAnswer++;
        scanf("%d", &some);
    }while(some != result);
    cout << "Count bad answer = " << badAnswer << endl;
    return 0;
}
