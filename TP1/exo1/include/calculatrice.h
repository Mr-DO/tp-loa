#ifndef CALCULATRICE_H
#define CALCULATRICE_H


class calculatrice
{
    public:
        calculatrice(int x1, int x2);
        int getResult();
        virtual ~calculatrice();

    protected:

    private:
        calculatrice();
        int x1;
        int x2;
};

#endif // CALCULATRICE_H
